import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import mongoose, { ConnectOptions } from 'mongoose';
import config from './config';
import fs from 'fs';
const routesDirectory = 'src/api/v1/routes';
const app = express();

app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
    methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH']
  })
);

app.use(cookieParser());
app.use(express.json());

mongoose.connect(
  `${config.databaseURL}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  } as ConnectOptions,
  err => {
    if (err) throw err;
    console.log('Mongoose is Connected');
  }
);

fs.readdirSync(routesDirectory, { withFileTypes: true }).forEach(function (
  file
) {
  require(`../${routesDirectory}/${file.name}`)(app);
});

app.listen(config.port, () => {
  console.log(`express server started at port ${config.port}`);
});

interface AuthDto {
  email: string;
  firstName: string;
  lastName: string;
  gender: string;
  password: string;
  confirmPassword: string;
}

export { AuthDto };

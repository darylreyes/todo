import UserService from '../services/User';

const index = async (req, res) => {
  const userInstance = new UserService();
  const users = await userInstance.getUsers();
  res.status(200).send({ users });
};
const show = async (req, res) => {
  const id = req.params.id;
  const userInstance = new UserService();
  const user = await userInstance.getUser(id);
  res.status(200).send({ user });
};

const update = async (req, res) => {
  const id = req.params.id;
  const userInstance = new UserService();
  await userInstance.updateUser({ id, data: req.body });
  res.status(200).send({ message: 'User updated successfully' });
};
const destroy = async (req, res) => {
  const id = req.params.id;
  const userInstance = new UserService();
  await userInstance.deleteUser({ id });

  res.status(200).send({ message: 'User deleted successfully' });
};

export = {
  index,
  show,
  update,
  destroy
};

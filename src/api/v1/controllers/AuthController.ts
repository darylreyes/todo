import RegisterValidation from '../validations/RegisterValidation';
import User from '../models/User';
import Profile from '../models/Profile';
import bcrypt from 'bcryptjs';
import { AuthDto } from '../interfaces';
import UserService from '../services/User';
import AuthService from '../services/Auth';
const register = async (req, res) => {
  try {
    const userDetails: AuthDto = req.body;

    const error = RegisterValidation(req.body);
    if (Object.keys(error).length)
      return res.status(400).json({
        message: error
      });
    const user = await User.findOne({ email: userDetails.email }).exec();

    if (user) {
      return res.status(400).json({
        message: 'Email already exists'
      });
    }
    const userInstance = new UserService();
    await userInstance.createUser(userDetails);

    res.status(200).json({
      message: 'Account Successfully Created'
    });
  } catch (error) {
    console.log('#### ERROR', error);
    return res.status(500).json(error);
  }
};
const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userInstance = new AuthService();
    const token = await userInstance.login({ email, password });
    res.cookie('todo-jwt', token, { httpOnly: true });
    res.status(200).send({ token });
  } catch (error: any) {
    const status = error.statusCode || 500;
    res.status(status).json({ message: error.message });
  }
};
const isAuth = async (req, res) => {
  try {
    const userId = req.user.id;
    console.log(req.user);
    const userInstance = new UserService();
    const user = await userInstance.getUser(userId);
    console.log(user);
    res.status(200).json({ user });
  } catch (error: any) {
    const status = error.statusCode || 500;
    res.status(status).json({ message: error.message });
  }
};

export = {
  register,
  login,
  isAuth
};

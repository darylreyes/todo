import mongoose from 'mongoose';
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user'
  },
  status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
  },
  profile: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }
});

export default mongoose.model('User', UserSchema);

import mongoose, { Schema } from 'mongoose';
const ProfileSchema = new mongoose.Schema({
  firstName: String,

  lastName: String,

  gender: {
    type: String,
    enum: ['male', 'female']
  },

  birthday: String,

  age: Number,

  location: Schema.Types.Mixed
});

export default mongoose.model('Profile', ProfileSchema);

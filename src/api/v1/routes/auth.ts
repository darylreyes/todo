import auth from '../middlewares/auth';
import AuthController from '../controllers/AuthController';
module.exports = app => {
  app.post('/v1/register/', AuthController.register);
  app.post('/v1/login/', AuthController.login);
  app.get('/v1/isAuth', auth, AuthController.isAuth);
};

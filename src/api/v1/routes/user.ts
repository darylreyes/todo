import auth from '../middlewares/auth';
import UserController from '../controllers/UserController';
module.exports = app => {
  app.get('/v1/users/', UserController.index);
  app.get('/v1/users/:id', UserController.show);
  app.put('/v1/users/:id', UserController.update);
  app.delete('/v1/users/:id', UserController.destroy);
};

import jwt from 'jsonwebtoken';
export default (req, res, next) => {
  try {
    const token = req.cookies['todo-jwt'];
    const options = process.env.APP_SECRET
      ? {}
      : {
          algorithms: ['RS256']
        };
    const decoded = jwt.verify(token, process.env.APP_SECRET, options);
    req.user = { ...decoded };
  } catch (error) {
    throw Object.assign(new Error('Unauthorized'), { statusCode: 403 });
  }
  next();
};

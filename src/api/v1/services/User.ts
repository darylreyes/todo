import User from '../models/User';
import Profile from '../models/Profile';
import bcrypt from 'bcryptjs';
export default class UserService {
  user: typeof User;
  profile: typeof Profile;
  constructor() {
    this.user = User;
    this.profile = Profile;
  }

  public async getUsers() {
    const users = await this.user.find({}).populate('profile');
    return users;
  }
  public async getUser(id) {
    const users = await this.user.findById(id).populate('profile');
    return users;
  }

  public async createUser({ firstName, lastName, gender, email, password }) {
    password = await bcrypt.hash(password, 12);
    const profile = await this.createProfile({ firstName, lastName, gender });
    const user = await new User({
      email,
      password,
      profile: profile._id
    }).save();

    return user;
  }
  public async createProfile({ firstName, lastName, gender }) {
    const profile = await new Profile({
      firstName,
      lastName,
      gender
    }).save();
    return profile;
  }

  public async updateUser({ id, data }) {
    const user = await this.user.findByIdAndUpdate(id, { $set: data }).exec();
    const profileId = user.profile.ty;
    await this.updateProfile({
      id: profileId,
      data
    });
  }

  public async updateProfile({ id, data }) {
    await this.profile.findByIdAndUpdate(id, { $set: data });
  }
  public async deleteUser({ id }) {
    try {
      const user = await this.user.findByIdAndDelete(id).exec();
      const profileId = user.profile;
      await this.deleteProfile({ id: profileId });
      console.log(user);
    } catch (error) {
      console.log(error);
    }
  }
  public async deleteProfile({ id }) {
    try {
      await this.profile.findByIdAndDelete(id).exec();
    } catch (error) {
      console.log(error);
    }
  }
}

import User from '../models/User';
import UserService from './User';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
import config from '../../../config';
export default class AuthService {
  user: typeof User;
  constructor() {
    this.user = User;
  }

  public async generateToken(user) {
    return await jwt.sign(
      {
        id: user.id,
        email: user.email,
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        role: user.role
      },
      config.appSecret,
      { expiresIn: '1h' }
    );
  }

  public async login({ email, password }) {
    const user = await this.user
      .findOne({ email })
      .select('+password')
      .populate('profile')
      .exec();
    if (!user) {
      throw Object.assign(new Error('User not found'), { statusCode: 404 });
    }
    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      throw Object.assign(new Error('User not found'), { statusCode: 404 });
    }

    const token = await this.generateToken(user);

    return token;
  }
}

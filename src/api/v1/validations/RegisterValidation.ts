import { AuthDto } from '../interfaces';

export default (data: AuthDto) => {
  const { email, password, firstName, lastName, confirmPassword } = data;
  const errors: any = {};

  if (!firstName) errors.firstName = 'First Name is Required';
  if (!lastName) errors.lastName = 'Last Name is Required';

  if (!email) {
    errors.email = 'Email is Required';
  } else {
    const regEx =
      /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (!email.match(regEx)) {
      errors.email = 'Email must be a valid email address';
    }
  }
  if (!password) {
    errors.password = 'Password must not empty';
  } else if (password !== confirmPassword) {
    errors.confirmPassword = 'Password must match to confirm password';
  }

  return Object.keys(errors).length ? errors : false;
};

import dotenv from 'dotenv';

dotenv.config();

export default {
  port: process.env.PORT || 3333,
  databaseURL: process.env.DATABASE_URI,
  appSecret: process.env.APP_SECRET
};
